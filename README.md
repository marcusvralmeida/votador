# Votador

Projeto desafio técnico Alterdata.

### Tecnologias utilizadas:
1. Backend:
    * Java 8
    * Maven
    * Spring Boot 
    * Spring Security
    * JWT
    * PostgreSQL
    * Docker 
    * Docker Compose

2. Frontend:
    * Angular 7
    * Bootstrap 4
    * Angular ClI

### Arquitetura:

O projeto é separado em dois módulos: "backend" e "frontend".

A arquitetura do projeto "backend" utilizada, foi o modelo simples de três camadas (Repository - Service Layer - Controller).

Para a camada de persistência (repository) utilizou-se a API JPA, implementada pelo Hibernate.

A camada de serviço (service layer) contém as classes com métodos transacionais que acessam a camada de persistência (repository).

A camada de controllers é responsável por expor o acesso as informações providas pela camada de serviço, através do protocolo HTTP.

Nesse projeto implementou-se a autenticação via token. Utilizando o padrão JWT. Suportado pelo Spring Security. 

O processo de autenticação do usuário é feito por uma requisição, usando o método POST, passando as credenciais do usuário (email e senha).
No retorno, se não hover erro, será retornado o token de acesso para posteriores requisições. 

O projeto frontend foi desenvolvido utilizando o framework Angular 7, e este se encontra na pasta "frontend" do projeto. 

O procedimento para realizar a construção do projeto frontend é realizar utilizando o Maven e o plugin para instalação de pacotes através no npm.

### Endpoints da API:

1. POST /auth/login

    Endpoint utilizado para realizar login e a obtenção do Token JWT.
    
    Exemplo: 
            
            POST /auth/login
            Request Body: {"email": "test@alterdata.com.br": "password": "12345678" }
            
            Response:
            {"token":"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBhbHRlcmRhdGEuY29tLmJyIiwiZXhwIjoxNTUzNzIxNTEzfQ.O1vNVyPI94uF52JDHWpGDNxFOOeuk6qY0rnoniyLiTTjAt9p-gBVwoAVERgKndIaVFhZvBrdlG2aa-oqrU_6oQ", "email":"admin@alterdata.com.br"}
            
            HTTP Response Header:
            Authorization: "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBhbHRlcmRhdGEuY29tLmJyIiwiZXhwIjoxNTUzNzIxNTEzfQ.O1vNVyPI94uF52JDHWpGDNxFOOeuk6qY0rnoniyLiTTjAt9p-gBVwoAVERgKndIaVFhZvBrdlG2aa-oqrU_6oQ"

    Obs.: Para acessar os demais endpoints será necessário enviar o Http Header Authorization, retornado no login.
    
    
2. GET|POST|DELETE /api/users/{id}
    
    Endpoint para buscar, criar e apagar usuários

    Exemplo: 
            
            GET /api/users
            
            Response:
            [
                {
                    "id":2,
                    "name":"Testador",
                    "email":"test@alterdata.com.br",
                    "roles":[
                        {"id":2,"name":"USER"}
                    ]
                }
            ]
    
    Exemplo:

            POST /api/users
            Request Body:
            {
                "name": "Usuário",
                "email": "t@test.com",
                "password": "password"
                "roles": [{
                    "id": 1, "name": "USER"
                }]
            }

            Response Body:
            {
                "id":3,
                "name":"Nome",
                "email":"te@t.com",
                "roles":[
                    "id": 1,
                    "name": "USER"
                ]
            }
            Http Status: 201 CREATED

    Obs.: 
    
    - Os atributos obrigatórios são:
        * email
        * password


3. GET|POST|PUT|DELETE /api/features/{id} 
    
    Endpoint para buscar, criar, atualizar e apagar recursos.

    Obs.: Esse endpoint segue o formato [HATEOAS](https://en.wikipedia.org/wiki/HATEOAS)

    Exemplo: 
            
            GET /api/features
            
            Response:
            {
                "_embedded" : {
                    "features" : [ {
                        "title" : "Feature 1",
                        "description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue commodo sapien, eleifend tempor ante.",                       
                        "_links" : {
                            "self" : {
                            "href" : "http://[::]:8090/api/features/1"
                            },
                            "feature" : {
                            "href" : "http://[::]:8090/api/features/1{?projection}",
                            "templated" : true
                            },
                            "votes" : {
                            "href" : "http://[::]:8090/api/features/1/votes{?projection}",
                            "templated" : true
                            }
                        }
                        } ]
                    },
                    "_links" : {
                        "self" : {
                        "href" : "http://[::]:8090/api/features{?page,size,sort,projection}",
                        "templated" : true
                        },
                        "profile" : {
                        "href" : "http://[::]:8090/api/profile/features"
                        }
                    },
                    "page" : {
                        "size" : 20,
                        "totalElements" : 1,
                        "totalPages" : 1,
                        "number" : 0
                    }
            }
    
    Exemplo:
            
            POST /api/features
            Request Body: 
            {
                "title": "Test Feature",
                "description": "Lorem ipsum dolor sit amet."
            }

    

    Obs.: 
    
    - Os atributos obrigatórios para salvar um recurso são:
        * title
        * description

    - Não existe restrição quanto a criar recursos (feature) com mesmo título.

4. POST /api/votes/{feature_id}

    Endpoint para submeter um voto para um recurso com um id (feturea_id)

    Exemplo:

        POST /api/votes/1
        Request Body: 
            { 
                "comment": "Lorem ipsum" 
            }

        Response: ''
        Http Status: 201 CREATED

    Obs.: Caso o usuário tentar votar num mesmo recurso mais de uma vez a resposta é a seguinte:

        Response Body: 
            {
                "error": "O usuário já votou!"
            }
        Http Status: 209 CONFLICT


### Como executar:
Existe um arquivo Makefile, na base do projeto, com algumas tasks para facilitar a execução do projeto:

1. Para executar o projeto utilizando docker e docker-componse:

        $ make run 

    Esse comando constroi o projeto frontend e o projeto backend, constroi uma imagem docker. E inicia essa imagem junto com uma imagem do PostgreSQL.
    
    Com o projeto executando, basta acessar http://localhost:8090/ para acessar o frontend.
    
    Todas as chamadas aos endpoints podem ser feitas a partir de http://localhost:8090/ .
2. Para executar o projeto backend em modo de desenvolvimento:
 
        $ make run_dev
        
Obs.: 
    
- Para utilizar essa task será necessário alterar as configurações de conexão com o banco de dados no arquivo [application.yml](https://bitbucket.org/marcusvralmeida/votador/src/master/backend/src/main/resources/application.yml).


- Para executar o projeto em ambiente Windows segue os passos:
    
    1. Acessar o projeto frontend e executar "mvn clean package"

    2. Acessar o projeto backend e executor "mvn spring-boot:run"

    * Atenção para as configurações da conexão com o banco de dados no arquivo application.yml

#### Para acessar o frontend:
1. Com o projeto em execução, acessar a [http://localhost:8090/](http://localhost:8090), será exibida a tela de login.

#### Observações:
1. No script de criação e carga do banco de  dados são criados dois usuários:

    - Administrador:
        
        email: admin@alterdata.com.br
        
        senha: password
    
    - Testador:

        email: test@alterdata.com.br

        senha: password

    O arquivo que contém o script de definição do banco de dados é o [backend/src/main/resources/schema.sql](https://bitbucket.org/marcusvralmeida/votador/src/master/backend/src/main/resources/schema.sql).
     
    Existe um arquivo onde é realizar a carga de dados inicial do banco de dados dor projeto, [backend/src/main/resources/data.sql](https://bitbucket.org/marcusvralmeida/votador/src/master/backend/src/main/resources/data.sql).
    
2. O sistema operacional utilizado para o desenvolvimento foi o Ubuntu.

3. Alguns pontos não foram implmentados como: o tratamento para votos em fuso horários diferentes e a implementação de testes (unitários, integração, e2e).