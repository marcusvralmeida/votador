insert into role(name) values('ADMIN');
insert into role(name) values('USER');
insert into users(name, email, password) values('Administrador','admin@alterdata.com.br','$2a$10$xAqXQO09aOK/DQPgCw47Vu/iHVbsfBHK0PPWphUCbzF.cnGyD845e');
insert into users_roles(user_id, roles_id) values(1, 1);
insert into users(name, email, password) values ('Testador', 'test@alterdata.com.br', '$2a$10$NJ1N5KXYc4GKwBcFwlIstOfGRvGqdPZsO/s3j7jxMYlqEh4qFmUf.');
insert into users_roles(user_id, roles_id) values(2, 2);

insert into features(id, title, description) values(1, 'Feature 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue commodo sapien, eleifend tempor ante.');
--insert into features(id, title, description) values(2, 'Feature 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue commodo sapien, eleifend tempor ante.');
--insert into features(id, title, description) values(3, 'Feature 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue commodo sapien, eleifend tempor ante.');
--insert into features(id, title, description) values(4, 'Feature 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue commodo sapien, eleifend tempor ante.');
