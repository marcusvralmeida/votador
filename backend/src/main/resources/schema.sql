create table if not exists features (
    id  bigserial not null,
    description varchar(255) not null,
    title varchar(255) not null,
    primary key (id)
);

create table if not exists users (
    id  bigserial not null,
    email varchar(255) not null,
    name varchar(255),
    password varchar(255) not null,
    primary key (id)
);

create table if not exists role (
    id  bigserial not null,
    name varchar(255) not null,
    primary key (id)
);

create table if not exists users_roles (
    user_id int8 not null,
    roles_id int8 not null,
    primary key (user_id, roles_id)
);

create table if not exists votes (
    id int8 not null,
    vote_comment varchar(255) not null,
    vote_date timestamp not null,
    feature_id int8 not null,
    user_id int8 not null,
    primary key (id)
);


alter table role add constraint unique_role_name unique (name);
alter table users add constraint unique_user_email unique (email);
alter table votes add constraint unique_vote unique (user_id, feature_id);
alter table users_roles 
       add constraint fk_users_roles_role_id 
       foreign key (roles_id) 
       references role;
alter table users_roles 
       add constraint fk_users_roles_user_id 
       foreign key (user_id) 
       references users;
alter table votes 
       add constraint fk_features_feature_id
       foreign key (feature_id) 
       references features;
alter table votes 
       add constraint fk_votes_user_id
       foreign key (user_id) 
       references users;

create sequence hibernate_sequence start 1;
