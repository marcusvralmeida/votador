package br.com.alterdata.votador.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.alterdata.votador.models.Role;

public interface RolesRepository extends JpaRepository<Role, Long> {

}
