package br.com.alterdata.votador.controllers;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alterdata.votador.models.User;
import br.com.alterdata.votador.services.UsersService;

@RestController
@RequestMapping("/api/users")
public class UsersController {
	
	@Autowired
	private UsersService userService;

	@PostMapping
	public ResponseEntity<?> create(@RequestBody @Valid User user) {
		return new ResponseEntity<>(userService.save(user), HttpStatus.CREATED); 
	}

	@GetMapping(path = { "/{id}" })
	public ResponseEntity<?> findOne(@PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>(userService.findById(id), HttpStatus.OK);
		}catch(EntityNotFoundException enf) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} 
	}

	@DeleteMapping(path = { "/{id}" })
	public ResponseEntity<?> delete(@PathVariable("id") Long id) {
		try {
			userService.delete(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (EntityNotFoundException enf) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	@GetMapping
	public ResponseEntity<?> findAll() {
		List<User> users = userService.findAll();
		if (users.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
}
