package br.com.alterdata.votador.services.impl;

import java.time.LocalDateTime;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.alterdata.votador.exceptions.AlreadyVotedException;
import br.com.alterdata.votador.models.Feature;
import br.com.alterdata.votador.models.User;
import br.com.alterdata.votador.models.Vote;
import br.com.alterdata.votador.repositories.UsersRepository;
import br.com.alterdata.votador.repositories.VotesRespository;
import br.com.alterdata.votador.services.VotesService;

@Service
public class VotesServiceImpl implements VotesService {
	
	@Autowired
	VotesRespository repository;
	
	@Autowired
	UsersRepository usersRepository;
	
    @Override
    public Vote vote(Long featureId, Vote vote) throws AlreadyVotedException {

    	try {
    		String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    		User user = usersRepository.findByEmail(email);
    		
    		Feature feature = new Feature(featureId);
    		vote.setFeature(feature);
    		vote.setDate(LocalDateTime.now());
    		vote.setUser(user);
    		return repository.save(vote);
    		
    	} catch(DataIntegrityViolationException ex) {
    		if(ex.getCause().getClass().equals(ConstraintViolationException.class)) {
    			throw new AlreadyVotedException("O usuário já votou!");
    		} else {
    			throw ex;
    		}
    		
    	}
    }
}
