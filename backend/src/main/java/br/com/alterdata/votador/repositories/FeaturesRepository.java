package br.com.alterdata.votador.repositories;

import br.com.alterdata.votador.models.Feature;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "features", path = "features")
public interface FeaturesRepository extends PagingAndSortingRepository<Feature, Long> {
}
