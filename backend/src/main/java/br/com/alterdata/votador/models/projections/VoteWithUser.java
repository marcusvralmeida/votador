package br.com.alterdata.votador.models.projections;

import java.time.LocalDateTime;

import org.springframework.data.rest.core.config.Projection;

import br.com.alterdata.votador.models.User;
import br.com.alterdata.votador.models.Vote;

@Projection(
		name="voteWithUser",
		types= {Vote.class})
public interface VoteWithUser {
	String getComment();
	
	LocalDateTime getDate();
	
	User getUser();
}
