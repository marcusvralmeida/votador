package br.com.alterdata.votador.models.projections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import br.com.alterdata.votador.models.Feature;

@Projection(
		name="featureWithNumberOfVotes",
		types= {Feature.class})
public interface FeatureWithNumberOfVotes {

    Long getId();

	String getTitle();

	String getDescription();

	@Value("#{target.getVotes().size()}")
	Integer getNumberOfVotes();

}
