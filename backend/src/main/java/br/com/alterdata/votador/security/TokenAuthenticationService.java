package br.com.alterdata.votador.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService {
		static Logger logger = LoggerFactory.getLogger(TokenAuthenticationService.class);

		// EXPIRATION_TIME = 10 dias
		static final long EXPIRATION_TIME = 860_000_000;
		static final String SECRET = "zji0gfE4qKtSA2GK1NjIS8HUS7gKxUhl";
		static final String TOKEN_PREFIX = "Bearer";
		static final String HEADER_STRING = "Authorization";

		public static void addAuthentication(HttpServletResponse response, String username){
			String JWT = Jwts.builder()
					.setSubject(username)
					.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
					.signWith(SignatureAlgorithm.HS512, SECRET)
					.compact();
			String authenticationToken = TOKEN_PREFIX + " " + JWT;
			response.addHeader(HEADER_STRING, authenticationToken);
			try {
				response.setContentType("application/json");
			    response.setCharacterEncoding("UTF-8");
			    response.getWriter().write("{\"token\":\"" + authenticationToken + "\", \"email\":\""+username+"\"}");
			}catch (IOException ex) {
				logger.error("Erro ao adicionar token a resposta do login");
			}

		}

		public static Authentication getAuthentication(HttpServletRequest request) {
			String token = request.getHeader(HEADER_STRING);

			if (token != null) {
				String user = Jwts.parser()
						.setSigningKey(SECRET)
						.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
						.getBody()
						.getSubject();

				if (user != null) {
					return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
				}
			}
			return null;
		}

}
