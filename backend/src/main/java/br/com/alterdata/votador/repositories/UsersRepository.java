package br.com.alterdata.votador.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.alterdata.votador.models.User;


public interface UsersRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u LEFT JOIN u.roles WHERE u.email = :email")
    User findByEmail(@Param("email") String email);
}
