package br.com.alterdata.votador.models;

public enum RoleName {
	 USER("USER"),
	 ADMIN("ADMIN");
	
	private final String label;
	
	RoleName(String label){
		this.label = label;
	}
	
	public String getLabel() {
		return this.label;
	}
}
