package br.com.alterdata.votador.services;

import java.util.List;

import br.com.alterdata.votador.models.User;

public interface UsersService {
	User save(User user);
	
    User update(User user);
    
    User delete(Long id);

    List<User> findAll();

    User findById(Long id);
    
    User findByEmail(String email);
}
