package br.com.alterdata.votador.services;
import br.com.alterdata.votador.exceptions.AlreadyVotedException;
import br.com.alterdata.votador.models.Feature;
import br.com.alterdata.votador.models.Vote;

public interface VotesService {
    Vote vote(Long featureId, Vote vote) throws AlreadyVotedException;
}
