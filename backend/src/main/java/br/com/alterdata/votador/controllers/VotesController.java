package br.com.alterdata.votador.controllers;

import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alterdata.votador.exceptions.AlreadyVotedException;
import br.com.alterdata.votador.models.Vote;
import br.com.alterdata.votador.services.VotesService;

@RestController
@RequestMapping("/api/votes")
public class VotesController {

	@Autowired
	VotesService service;

	@PostMapping("/{featureId}")
	public ResponseEntity<?> vote(@PathVariable("featureId") Long id, @Valid @RequestBody Vote vote) {
		try{
			service.vote(id, vote);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch(AlreadyVotedException ex) {
			HashMap<String, String> map = new HashMap<>();
			map.put("error", ex.getMessage());
			return new ResponseEntity<>(map, HttpStatus.CONFLICT);
		}
	}
}
