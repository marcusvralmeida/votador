package br.com.alterdata.votador.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.alterdata.votador.models.Vote;
import br.com.alterdata.votador.models.projections.VoteWithUser;

@RepositoryRestResource(excerptProjection = VoteWithUser.class)
public interface VotesRespository extends JpaRepository<Vote, Long> {
}
