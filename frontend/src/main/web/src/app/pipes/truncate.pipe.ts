import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  transform(value: string, length: string, ellipse: string) : string {
  
    let limit = length ? parseInt(length, 10) : 10;
    let trail = ellipse ? ellipse : '...';

    return value.length > limit ? value.substring(0, limit) + trail : value;
  }

}