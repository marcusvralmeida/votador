import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FeaturesService {
  
  constructor(private http: HttpClient) { }

  listFeatures() {
    const params = new HttpParams().set('projection', 'featureWithNumberOfVotes')
    return this.http.get<any>(`${environment.API_URL}/api/features`, {params});
  }

  detailsFeature(featureId: number){
    return this.http.get<any>(`${environment.API_URL}/api/features/${featureId}`);
  }

  voteInFeature(featureId: number, vote: any){
    return this.http.post<any>(`${environment.API_URL}/api/votes/${featureId}`, vote);
  } 
}
