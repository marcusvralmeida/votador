import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FeaturesService } from 'src/app/services/features.service';
import { Feature } from 'src/app/models/feature';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  public feature: Feature;
  private featureId: number;
  voteForm = new FormGroup({
    comment: new FormControl('',[Validators.required])
  });
  public modalRef: BsModalRef; 


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private featuresService: FeaturesService) { }

  ngOnInit() {
    this.loadFeature();
  }

  private loadFeature(){
    this.featureId = Number(this.route.snapshot.paramMap.get('id'));
    this.featuresService.detailsFeature(this.featureId).subscribe(data => {
      this.feature = data;
    });
  }

  get f() { return this.voteForm.controls; }

  public submitVote() {
    this.featuresService.voteInFeature(this.featureId, {comment: this.f.comment.value})
      .subscribe(data => {
        this.toastr.success('Voto computado com sucesso.','Feito!');
        this.modalRef.hide();
        this.loadFeature();
        this.voteForm.reset();
      },
      err =>{
        if(err.status===409) {
          this.toastr.warning('Você já votou nesse recurso.','Aviso!');
        } else {
          this.toastr.error("Ocorreu um erro interno.", "Erro!");
        }
        this.voteForm.reset();
        this.modalRef.hide();
      });
  }

  public openModal(template: TemplateRef<any>, feature: Feature) {
    this.modalRef = this.modalService.show(template); 
    this.feature = feature;
  }

}
