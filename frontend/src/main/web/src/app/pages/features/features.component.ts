import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Feature } from 'src/app/models/feature';
import { FeaturesService } from 'src/app/services/features.service';
import { ToastrService } from 'ngx-toastr';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/public_api';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {
  public features : Feature[];
  public page: Feature[];
  public selectedFeature : Feature;
  public itemsPerPage = 10;

  voteForm = new FormGroup({
    comment: new FormControl('',[Validators.required])
  });

  public modalRef: BsModalRef; 
  constructor(
    private featuresService: FeaturesService,
    private router: Router,
    private toastr: ToastrService,
    private modalService: BsModalService) {
  }

  ngOnInit() {
    this.loadFeatures();
  }

  get f() { return this.voteForm.controls; }

  private loadFeatures() {
    this.featuresService.listFeatures().subscribe(data => {
      this.features = data._embedded.features;
      this.page = this.features.slice(0, this.itemsPerPage);
    });
  }
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.page = this.features.slice(startItem, endItem);
  }

  public openModal(template: TemplateRef<any>, feature: Feature) {
    this.modalRef = this.modalService.show(template); 
    this.selectedFeature = feature;
  }

  public detailFeature(feature){
    this.router.navigate([`/feature/${feature.id}` ]);
  }

  public submitVote() {
    this.featuresService.voteInFeature(this.selectedFeature.id, {comment: this.f.comment.value})
      .subscribe(data => {
        this.toastr.success('Voto computado com sucesso.','Feito!');
        this.modalRef.hide();
        this.loadFeatures();
        this.voteForm.reset();
      },
      err =>{
        if(err.status===409) {
          this.toastr.warning('Você já votou nesse recurso.','Aviso!');
        } else {
          this.toastr.error("Ocorreu um erro interno.", "Erro!");
        }
        this.voteForm.reset();
        this.modalRef.hide();
      });
  }
}
