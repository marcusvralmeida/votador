import { User } from './user';

export class Vote {
    comment: string;
    date: Date;
}