import { Vote } from './vote';

export class Feature {
    id: number;
    title: string;
    description: string;
    numberOfVotes: number;
    votes: Vote[];
}