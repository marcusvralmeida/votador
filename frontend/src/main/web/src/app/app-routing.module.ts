import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { FeaturesComponent } from './pages/features/features.component';
import { AuthGuard } from './guards/auth.guard';
import { DetailsComponent } from './pages/details/details.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: FeaturesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'feature/:id',
    component: DetailsComponent,
    canActivate: [AuthGuard]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
