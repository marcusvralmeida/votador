PRJ_DIR=$(PWD)

build_front:
	cd $(PRJ_DIR)/frontend/ && mvn clean package

build_api:
	cd $(PRJ_DIR)/backend/ && mvn clean package
	
run: build_front build_api
	cd $(PRJ_DIR)/backend/ && docker-compose up

run_dev: build_front
	cd $(PRJ_DIR)/backend/ && mvn spring-boot:run

remove_containers:
	cd $(PRJ_DIR)/backend/ && docker-compose down && docker rmi backend:latest
